# Importing binaries with the Percussion batch importer

# Distribution Contents

- **spring-custom.xml** - The custom spring configuration for this example.
- **batch_importer_binary_test/** - The directory containing the binary content to be imported.
- **Sample.xml** - The input file containing the XML representation of the item to be imported.
- **README.md** - This document.
- **videoDocumentation.zip** - A series of video tutorials covering the configuration and use of the batch importer.

## Setup

### Assumptions

This example was tested with a new install of Rhythmyx on a Windows virtual machine. While it is may be possible to setup this example with an existing Rhythmyx installation, system specific configurations may affect the outcome.

#### Create the *batchImporterBinaryTest* content type

1. Click the 'Content Design' tab from the left pane
2. Right click on 'Content Types -> New -> Content Type'
3. Set:
    - Content Type name = batchImporterBinaryTest
    - Label = Batch Importer Binary Test
4. Move all entries from 'Available Communities' to 'Visible to these communities'
5. Moving 'Simple Workflow' from 'Available Workflows' to 'Allowed Workflows'
6. Select 'Simple Workflow' as the 'Default Workflow'
7. Click 'Finish'
8. Open the 'batchImporterBinaryTest' content type.
9. Add the 'apdf' field:
  - Set:
      - Name: apdf, Label: A PDF:, Control: sys_File, Source: Local
  - Check the 'Required' checkbox
10. Add the 'apdf_filename' field:
  - Set:
      - Name: apdf_filename, Label: A PDF Filename:, Control: sys_EditBox, Source: Local
  - Check the 'Required' checkbox
11. Add the 'apdf_ext' field:
  - Set:
      - Name: apdf_ext, Label: A PDF Extension:, Control: sys_EditBox, Source: Local
  - Check the 'Required' checkbox
11. Add the 'sys_suffix' field:
  - Click 'System -> sys_suffix' from 'Shared and system fields' and click the right-arrow to add it as a field
12. Add the 'filename' field:
  - Click 'Shared -> shared -> filename' from 'Shared and system fields' and click the right-arrow to add it as a field
13. Click the 'Properties' tab (at the bottom)
14. Select the 'Pre-Processing' tab in the 'Pre-Processing' section.
15. Click the first row in the 'Extension Column' and choose 'sys_imageInfoExtractor' from the select control
16. Click the second row in the 'Extension Column' and choose 'sys_CopyParameter' from the select control
      - Click the select box in the second column and set:
        - source = apdf_filename
        - destination = filename
17. Click the third row in the 'Extension Column' and choose 'sys_CopyParameter' from the select control
      - Click the select box in the second column and set:
        - source = apdf_ext
        - destination = sys_suffix
18. Click the save button.
19. Restart the Rhythmyx server for the changes to take effect.

#### Create the *batchImporterBinaryTest* template

1. Click the 'Assembly Design' tab from the left pane
2. Expand 'Templates'
3. Right click on 'Shared > New > Template'
4. Click the 'Shared' radio button and click 'Next'
5. Set:
    - Assembler = binaryAssembler
6. Click 'Next'
7. Set:
    - Template name = batchImporterBinaryTest
8. Move all entries from 'Available Communities' to 'Visible to these communities'.
9. Click 'Next'
10. Click 'Next'
11. Move 'batchImporterBinaryTest' from 'Available Content Types' to 'Associated Content Types'.
12. Click 'Finish'
13. Click the 'Bindings' tab (at the bottom)
14. Click the first row under 'Variables'
  - Set:
      - Variable Name = $sys.binary
      - JEXL Expression = $sys.item.getProperty("apdf");
15. Click the second row under 'Variables'
  - Set:
      - Variable Name = $sys.mimetype
      - JEXL Expression = "application/pdf"
17. Click the 'Save' button
18. Restart the Rhythmyx application server for the changes to take effect.

#### Configure the PSO Batch Importer

1. Clone the [PSOBatchImporter](https://github.com/percussion/PSOBatchImporter).
2. Rename *PSOBatchImporter/main/content/input/Sample.xml* to *PSOBatchImporter/main/content/input/Sample.xml.original*.
3. Copy *Sample.xml* included in the root of this distribution to *PSOBatchImporter/main/content/input*.
4. Copy *spring-custom.xml* included in the root of this distribution to *PSOBatchImporter/main/config*.
5. Copy the *batch_importer_test* folder included in the root of this distribution to *PSOBatchImporter/main/content*.

## Running the batch importer

1. Run the *PSOBatchImporter/main/import.bat* script from a cmd shell.
2. Once the batch importer has finished, log into the Content Explorer and verify there is a new content item titled 'Batch Importer Test', an *Images* folder with one image and a *Files* folder with one pdf file.
3. You can also preview the item in the content explorer using the *batchImportBinaryTest* template from above.

## Important details

The sole purpose of this example is to demonstrate how to configure the batch importer to handle binary content. Users are encouraged to watch the training videos included in this distribution for a more complete understanding.

For purposes of this example, take note of the following files and sections.

***spring-custom.xml***

The batch importer will correctly import binary (file) field types that are configured for its *fileProcessor*. The configuration for this example is as follows:

```xml
<bean class="com.percussion.pso.rxws.item.processor.impl.FileProcessor" id="fileProcessor">
  <property name="tempdir" value="content/TEMP"/>
  <property name="fileBase" value="content/batch_importer_test"/>
  <property name="fields">
    <map>
      <entry key="batchImporterBinaryTest.apdf">
        <map>
          <entry key="type" value="file"/>
        </map>
      </entry>
    </map>
  </property>
</bean>
```

- In this example, we are configuring the *apdf* field for the *batchImporterBinaryTest* content type.

- Take note of the *filebase* property. This sets the location of the directory containing the content (the PDFs) that will be imported into Rhythmyx. This could be a relative path or an absolute path on the filesystem.

**Sample.xml**

This example imports two PDF (binary) content items.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<importer>
  <item communityName="Enterprise_Investments" importRoot="//Sites/EnterpriseInvestments/BatchImporterBinaryTest" keyField="sys_title" type="batchImporterBinaryTest">
    <paths>
      <path>//Sites/EnterpriseInvestments/BatchImporterBinaryTest</path>
    </paths>
    <fields>
      <field name="sys_title" value="I See You"/>
      <field name="apdf" value="files/iseeyou.pdf"/>
    </fields>
  </item>
  <item communityName="Enterprise_Investments" importRoot="//Sites/EnterpriseInvestments/BatchImporterBinaryTest" keyField="sys_title" type="batchImporterBinaryTest">
    <paths>
      <path>//Sites/EnterpriseInvestments/BatchImporterBinaryTest</path>
    </paths>
    <fields>
      <field name="sys_title" value="Sauron"/>
      <field name="apdf" value="files/sauron.pdf"/>
    </fields>
  </item>
</importer>
```
